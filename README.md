Sudoku checker
=======================

Task
===============

	Write a function done_or_not passing a board (list[list_lines]) as parameter.
	If the board is valid return 'Finished!', otherwise return 'Try again!'
